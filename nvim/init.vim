call plug#begin()

Plug 'Valloric/YouCompleteMe'
Plug 'w0rp/ale'
Plug 'nvie/vim-flake8'
Plug 'morhetz/gruvbox'
Plug 'airblade/vim-gitgutter'
Plug 'pangloss/vim-javascript'
Plug 'Quramy/tsuquyomi'
Plug 'elzr/vim-json'
Plug 'mxw/vim-jsx'
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }

call plug#end()

au BufNewFile,BufRead *.py
    \  set tabstop=4
    \| set softtabstop=4
    \| set shiftwidth=4
    \| set textwidth=80
    \| set expandtab
    \| set autoindent
    \| set fileformat=unix

au BufNewFile,BufRead *.html
    \  set tabstop=2
    \| set softtabstop=2
    \| set shiftwidth=2
    \| set expandtab
    \| set autoindent

au BufNewFile,BufRead *.css
    \  set tabstop=2
    \| set softtabstop=2
    \| set shiftwidth=2
    \| set expandtab
    \| set autoindent
    \| set textwidth=80

au BufNewFile,BufRead *.js
    \  set tabstop=2
    \| set softtabstop=2
    \| set shiftwidth=2
    \| set expandtab
    \| set autoindent
    \| set textwidth=80

au BufNewFile,BufRead *.jsx
    \  set tabstop=2
    \| set softtabstop=2
    \| set shiftwidth=2
    \| set expandtab
    \| set autoindent
    \| set textwidth=80

let python_highlight_all=1
syntax on
set cursorline
set termguicolors
set cc=80
set nu
set updatetime=100
set background=dark
let g:deoplete#enable_at_startup = 1
let g:gruvbox_italic=1
set hidden
let g:racer_cmd = "/Users/pjlast/.cargo/bin/racer"
colorscheme gruvbox
